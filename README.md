### Dependencies? ###

* AnypointStudio with the following runtimes: 3.8.1 CE or 3.8.1 EE
* Docker
* MySQL (To install the MySQL in docker run the following command in your terminal: sudo docker run --name mysql -e MYSQL_ROOT_PASSWORD=admin -d -p 3306:3306 mysql:5.6)
* MySQL Workbench to help you to create the tables and the functions (To install the Workbench follow this steps: http://www.edivaldobrito.com.br/como-instalar-o-instalar-mysql-workbench-no-ubuntu-e-derivados/)

### Creating database and tables ###
* Open your MySQL Workbeanch and select your mySQL connections instance, the default password is admin
* Run all the sql commands inside the file pokemonDatabase.sql located into the folder "/pokemon-register-api/src/main/resources/database/"

### Importing project into Anypoint Studio ###

* Select File -> Import..., then select Maven-based Mule Project from pom.xml type, it's located inside of Anypoint Studio's folder
* Search for the pom file inside this project, it is located in the root folder
* Click in the Finish button and wait for the project to install all the dependencies

* You'll need MySQL to run this project locally (Check the dependencies to see how to install the database)
